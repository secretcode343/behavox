#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Index (upload) Enron email corpus (in JSON format) to AWS Elastisearch
"""

import sys
assert sys.version_info[:2]==(2, 7), 'This script has only been tested in Python 2.7'

#CORPUS_ROOT = "enron_mail_20150507/maildir"
INPUT_JSON = "enron_mail_1.json"
# messing about with elasticsearch module

ES_DOMAIN = "https://search-behavox-customersuccess-acklracdqeerlo2yguoiosemwq.us-east-2.es.amazonaws.com/"

import sys

import certifi
certifi.where() # avoid Elasticsearch complaint 

import urllib3
urllib3.disable_warnings() # NOTE not for production code

import requests

from elasticsearch import Elasticsearch
from elasticsearch import helpers as Esh
es = Elasticsearch(ES_DOMAIN)

import util

INDEX = 'enron5'
MAPPING = 'mappings1.json'
with open(MAPPING,'rb') as f:
	mapping = f.read()

print "deleting %s" % INDEX
es.indices.delete(INDEX, ignore=404)
print "creating %s" % INDEX
es.indices.create(INDEX, body=mapping)
print "created %s" % INDEX

CHUNKSIZE = 1000
CHUNKBYTES = 10*1024*1024
n=0
def callback(x):
	global n
	n+=1
	#if n%CHUNKSIZE==0:
	#	util.progress(format(n, ' 7d'), every=CHUNKSIZE)
	# TODO add time & rate 	
	util.progress('Docs: %7d Elapsed: %s' % (n, util.elapsed()), every=CHUNKSIZE)
	return '{"index": {}}', x

util.start()
print "indexing..."
with open(INPUT_JSON, 'rb') as f:
	print "   indexing..."
	resp = Esh.bulk(es, f, index=INDEX, doc_type='message', 
		expand_action_callback=callback,
		chunk_size=CHUNKSIZE, max_chunk_bytes=CHUNKBYTES, request_timeout=30)
print
print resp
print "indexing done"
util.end(n)
