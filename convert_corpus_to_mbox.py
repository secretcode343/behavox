#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Convert Enron email corpus (downloaded and unpacked) to Elastisearch-compatible JSON

Liberal use of "Mining the Social Web, 2nd Edition" Chapter 6: Mining Mailboxes
"""

import sys
assert sys.version_info[:2]==(2, 7), 'This script has only been tested in Python 2.7'

CORPUS_ROOT = "enron_mail_20150507/maildir"
OUTPUT_MBOX = "enron_mail.mbox" # NOTE: will be deleted (truncated) and recreated 

import os
import email
import re
from time import asctime
from dateutil.parser import parse 
from timeit import default_timer as timer


def pad(s, max_width=80):
	return (s + ' '*max_width)[:max_width]

def progress_message(s):
	print pad(' '.join(s)), '\r',
	sys.stdout.flush()

p_count = 0
EVERY = 100
def progress(*s):
	global p_count
	p_count += 1
	if p_count%EVERY==0:
		progress_message(s)


start = timer()

n = 0
with open(OUTPUT_MBOX,'wb') as mbox:
	for root, dirs, files in os.walk(CORPUS_ROOT):
		#if n>10000: break # testing
		dirs.sort() # in-place
		#if dirs:
		#	print dirs
		#if files:
		#	print files
		for file_name in files:
			file_path = os.path.join(root, file_name)
			n += 1
			MAXW = 60
			progress(format(n, ' 10d'), file_path)
			#print format(n, ' 10d'), file_path, " "*30, "\r",
			#sys.stdout.flush()
			message_text = open(file_path, 'r').read()
			# Compute fields for the From_ line in a traditional mbox message

			_from = re.search(r"From: ([^\r]+)", message_text).groups()[0]
			_date = re.search(r"Date: ([^\r]+)", message_text).groups()[0]

			# Convert _date to the asctime representation for the From_ line

			_date = asctime(parse(_date).timetuple())

			msg = email.message_from_string(message_text)
			msg.set_unixfrom('From %s %s' % (_from, _date))

			mbox.write(msg.as_string(unixfrom=True) + "\n\n")

end = timer()
interval = end - start
print
print 'Completed %d files in %.03fs (%.06fs each)' % (n, interval, interval/n)

