#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Utilities
Progress messages and timing
"""

import sys
assert sys.version_info[:2]==(2, 7), 'This script has only been tested in Python 2.7'

from timeit import default_timer as timer

def pad(s, max_width=80):
	return (s + ' '*max_width)[:max_width]

def progress_message(s):
	print pad(s), '\r',
	sys.stdout.flush()

p_count = 0
def progress(s, every=100):
	global p_count
	p_count += 1
	if p_count%every==0:
		progress_message(s)

start = None
def start():
	global start
	start = timer()

def elapsed():
	return '%.03fs' % (timer()-start)

def end(count):
	end = timer()
	interval = end - start
	print
	print 'Completed %d msgs in %.03fs (%.06fs each)' % (count, interval, interval/count)

