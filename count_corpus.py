#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Count messages in Enron email corpus 
"""

import sys
assert sys.version_info[:2]==(2, 7), 'This script has only been tested in Python 2.7'

CORPUS_ROOT = "enron_mail_20150507/maildir"

import os
#import email
#import re
#from time import asctime
#from dateutil.parser import parse 
from collections import defaultdict

total = 0
total_user = defaultdict(int)
total_folder = defaultdict(int)

for user_dir in os.listdir(CORPUS_ROOT):
	#print user_dir
	user_path = os.path.join(CORPUS_ROOT, user_dir)
	for root, dirs, files in os.walk(user_path):
		folder_path = os.path.relpath(root, user_path) # TODO make relative
		#print "folder_path %s has %d dirs %d files" % (folder_path, len(dirs), len(files))
		
		msgs = len(files)
		total += msgs
		total_user[user_dir] += msgs
		total_folder[folder_path] += msgs
		
		print format(total, ' 10d'), "\r",
		sys.stdout.flush()

print "Total messages: %d" % total
print
for user_dir, count in sorted(total_user.iteritems(), key=lambda (k, v): -v)[:10]:
	print "Total for user %s: %d" % (user_dir, count)
print
for folder_path, count in sorted(total_folder.iteritems(), key=lambda (k, v): -v)[:10]:
	print "Total for folder path %s: %d" % (folder_path, count)

	#for folder in os.listdir(f):
		##print folder
		#f2 = os.path.join(f, folder)
		#if os.path.isfile(f2):
			#print "uh oh - regular file at folder level: ", f2
		#for subfolder in os.listdir(f2):
			#f3 = os.path.join(f2, subfolder)
			#if os.path.isdir(f3):
				#print "uh oh - subfolder: ", f3
	#if dirs:
	#	print dirs
	#if files:
	#	print files
	#for file_name in files:
	#	file_path = os.path.join(root, file_name)
	#	n += 1
	#	print format(n, ' 10d'), file_path, " "*30, "\r",
	#	sys.stdout.flush()


