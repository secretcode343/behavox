#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Convert Enron email corpus in mbox format to Elastisearch-compatible JSON

Not for distribution
Liberal use of "Mining the Social Web, 2nd Edition", Matthew Russell, Chapter 6: Mining Mailboxes
See LICENCE.txt
"""

import sys
assert sys.version_info[:2]==(2, 7), 'This script has only been tested in Python 2.7'

#CORPUS_ROOT = "enron_mail_20150507/maildir"
INPUT_MBOX = "enron_mail.mbox" 
OUTPUT_JSON = "enron_mail.json" # NOTE: will be deleted (truncated) and recreated 

#import os
import email
import json
import mailbox
import quopri
from time import mktime
from dateutil.parser import parse 
from BeautifulSoup import BeautifulSoup


def cleanContent(msg):
    # Decode message from "quoted printable" format
    msg = quopri.decodestring(msg)
        
    # Strip out HTML tags, if any are present.
    # Bail on unknown encodings if errors happen in BeautifulSoup.
    try:
        soup = BeautifulSoup(msg)
    except:
        return ''
    return ''.join(soup.findAll(text=True))


class Encoder(json.JSONEncoder):
    def default(self, o): return  list(o)

# generator 
def gen_json_msgs(mb):
    while 1:
        msg = mb.next()
        if msg is None:
            break
        yield jsonifyMessage(msg)


def jsonifyMessage(msg):
    json_msg = {'parts': []}
    for (k, v) in msg.items():
        json_msg[k] = v.decode('utf-8', 'ignore')

    # The To, Cc, and Bcc fields, if present, could have multiple items.
    # Note that not all of these fields are necessarily defined.

    for k in ['To', 'Cc', 'Bcc']:
        if not json_msg.get(k):
            continue
        json_msg[k] = json_msg[k].replace('\n', '').replace('\t', '').replace('\r', '')\
                                 .replace(' ', '').decode('utf-8', 'ignore').split(',')

    for part in msg.walk():
        json_part = {}
        if part.get_content_maintype() == 'multipart':
            continue
            
        json_part['contentType'] = part.get_content_type()
        content = part.get_payload(decode=False).decode('utf-8', 'ignore')
        json_part['content'] = cleanContent(content)
           
        json_msg['parts'].append(json_part)
        
    then = parse(json_msg['Date'])
    millis = int(mktime(then.timetuple())*1000 + then.microsecond/1000)
    json_msg['Date'] = {'$date' : millis}

    return json_msg



from timeit import default_timer as timer

def pad(s, max_width=80):
	return (s + ' '*max_width)[:max_width]

def progress_message(s):
	print pad(' '.join(s)), '\r',
	sys.stdout.flush()

p_count = 0
EVERY = 100
def progress(*s):
	global p_count
	p_count += 1
	if p_count%EVERY==0:
		progress_message(s)


start = timer()

mbox = mailbox.UnixMailbox(open(INPUT_MBOX, 'rb'), email.message_from_file)

# Write each message out as a JSON object on a separate line
with open(OUTPUT_JSON, 'w') as f:
	n = 0
	for msg in gen_json_msgs(mbox):
		if msg != None:
			n += 1
			progress(format(n, ' 10d'), msg['From'])
			f.write(json.dumps(msg, cls=Encoder) + '\n')

end = timer()
interval = end - start
print
print 'Completed %d msgs in %.03fs (%.06fs each)' % (n, interval, interval/n)

